<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Location;
use DB;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::paginate(2);

        // $Categorys = DB::table('categorys')->orderBy('id','desc')->get();

        return view('category.index',compact('categories'));
    }

    public function create()
    {
        $locations = Location::all();

        return view('category.create',compact('locations'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
        ]);

        $category = Category::create([
            'name' => $request->name,
            'code' => $request->code,
            'description' => $request->description,
        ]);

        $category->Location()->sync($request->location_id);

        return redirect()->back();
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('category.edit',compact('category'));
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
        ]);

        $category = Category::find($id);
        $category->fill($request->all());
        $category->save();

        return redirect('/category');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect('/category');
    }

}
