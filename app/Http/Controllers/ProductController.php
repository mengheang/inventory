<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Location;
use DB;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::with('category')->paginate(2);

        // $products = DB::table('products')->orderBy('id','desc')->get();

        return view('product.index',compact('products'));
    }

    public function create()
    {
        $locations = Location::all();

    	return view('product.create',compact('locations'));
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'name' => 'required',
            'code' => 'required',
            'description' => 'required',
            'photo' => 'mimes:jpeg,png'
    	]);

    	$product = Product::create([
    		'name' => $request->name,
    		'code' => $request->code,
    		'description' => $request->description,
    	]);

        $photo = request()->file('photo');
        $photo_name = time().'.'.$photo->getClientOriginalExtension();

        $photo->move(public_path('/photo'),$photo_name);

        $product->photo = $photo_name;
        $product->save();

        $product->Location()->sync($request->location_id);

    	return redirect()->back();
    }

    public function edit($id)
    {
        $product = Product::find($id);

        return view('product.edit',compact('product'));
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
        ]);

        $product = Product::find($id);
        $product->fill($request->all());
        $product->save();

        return redirect('/product');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('/product');
    }

}
