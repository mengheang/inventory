<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function product()
    {
    	return $this->belongsToMany('App\Product');
    }

    public function user()
    {
    	return $this->hasMany('App\User');
    }

    public function products()
    {
    	return $this->hasManyThrough('App\Product','App\User');
    }


}
