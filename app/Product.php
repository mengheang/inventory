<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','code','description'];

    public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function location()
	{
		return $this->belongsToMany('App\Location');
	}
}
