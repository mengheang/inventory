@extends('layouts.app')


@section('content')
<div class="container">
<h3>Product</h3>

@if($errors->any())
	<div class="alert alert-danger">Something wrong with fields belows</div>
@endif

<div class="card">
  <div class="card-header">
    Add New
  </div>
  <div class="card-body">
  	<form action="{{ url('/product') }}" method="post">
  		@csrf
	  <div class="form-group ">
	    <label for="formGroupExampleInput">Name</label>
	    <input type="text"  name="name" value="{{ old('name') }}" class="form-control @if($errors->has('name')) is-invalid @endif" id="formGroupExampleInput" placeholder="Enter product name">
	    <div class="invalid-feedback"> 
		    @if($errors->has('name')) {{ $errors->first('name') }} @endif
		</div>
	  </div>
	  <div class="form-group">
	    <label for="formGroupExampleInput2">Code</label>
	    <input type="text" name="code" value="{{ old('code') }}" class="form-control @if($errors->has('code')) is-invalid @endif" id="formGroupExampleInput2" placeholder="Enter product code">
	    <div class="invalid-feedback"> 
		    @if($errors->has('code')) {{ $errors->first('code') }} @endif
		</div>
	  </div>
	  <div class="form-group">
	    <label for="formGroupExampleInput2">Locations</label>
	    <select class="form-control" name="location_id[]" multiple="">
	    	@foreach($locations as $location)
	    		<option value="{{ $location->id }}">{{ $location->name }}</option>
	    	@endforeach
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="formGroupExampleInput2">Description</label>
	    <textarea name="description" id="summernote" class="form-control @if($errors->has('description')) is-invalid @endif" placeholder="Enter additional infomation here">
	    	{{ old('description') }}
	    </textarea>
	    <div class="invalid-feedback"> 
		    @if($errors->has('description')) {{ $errors->first('description') }} @endif
		</div>
	  </div>
	   <div class="form-group">
	   		<button type="submit" class="btn btn-primary">Submit</button>
	   </div>
	</form>
  </div>
</div>
</div>

@endsection