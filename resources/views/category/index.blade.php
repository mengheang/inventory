@extends('layouts.app')


@section('content')
<div class="container">
	<h3>Categories</h3>
	<div class="card">
	  <div class="card-header">
	    List
		<a href="{{ url('/category/create') }}" class="btn btn-success float-right">Add New</a>
	  </div>
	  <div class="card-body">
	  	<table class="table">
			<thead>
		  		<tr>
	  				<th>Name</th>
	  				<th>Description</th>
	  				<th>Action</th>
		  		</tr>
			</thead>
			<tbody>
				@foreach($categories as $category)
					<tr>
						<td>{{ $category->name }}</td>
						<td>{{ $category->description }}</td>
						<td>
							<a href="{{ url('/category/'.$category->id ) }}" class="btn btn-info btn-sm float-left m-r-20">Edit</a>
							<form action="{{ url('/category/'.$category->id) }}" method="post">
						  		@csrf
							    {!! method_field('delete') !!}
								<button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
	  	</table>
	  	{{ $categories->links() }}
	  </div>
	</div>
</div>
@endsection