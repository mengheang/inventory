@extends('layouts.app')


@section('content')
<div class="container">
	<h3>Product</h3>
	<div class="card">
	  <div class="card-header">
	    List
		<a href="{{ url('/product/create') }}" class="btn btn-success float-right">Add New</a>
	  </div>
	  <div class="card-body">
	  	<table class="table">
			<thead>
		  		<tr>
	  				<th>#</th>
	  				<th>Image</th>
	  				<th>Name</th>
	  				<th>Code</th>
	  				<th>Category</th>
	  				<th>Location</th>
	  				<th>Description</th>
	  				<th>Action</th>
		  		</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
					<tr>
						<td>{{ $product->id }}</td>
						<td>
							<img src="{{ asset('/photo/'.$product->photo) }}" class="img-responsive" width="50px">
						</td>
						<td>{{ $product->name }}</td>
						<td>{{ $product->code }}</td>
						<td>{{ $product->category? $product->category->name : '' }}</td>
						<td>
							@foreach($product->Location as $location)
								<ul>
									<li>{{ $location->name }}</li>
								</ul>
							@endforeach
						</td>
						<td>{{ $product->description }}</td>
						<td>
							<a href="{{ url('/product/'.$product->id ) }}" class="btn btn-info btn-sm float-left m-r-20">Edit</a>
							<form action="{{ url('/product/'.$product->id) }}" method="post">
						  		@csrf
							    {!! method_field('delete') !!}
								<button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
	  	</table>
	  	{{ $products->links() }}
	  </div>
	</div>
</div>
@endsection