@extends('layouts.app')


@section('content')
<div class="container">
<h3>Product</h3>

@if($errors->any())
	<div class="alert alert-danger">Something wrong with fields belows</div>
@endif

<div class="card">
  <div class="card-header">
    Add New
  </div>
  <div class="card-body">
  	<form action="{{ url('/product') }}" method="post">
  		@csrf
	  <div class="form-group ">
	    <label for="formGroupExampleInput">Name</label>
	    <input type="text"  name="name" value="{{ old('name') }}" class="form-control @if($errors->has('name')) is-invalid @endif" id="formGroupExampleInput" placeholder="Enter product name">
	    <div class="invalid-feedback"> 
		    @if($errors->has('name')) {{ $errors->first('name') }} @endif
		</div>
	  </div>
	  <div class="form-group">
	    <label for="formGroupExampleInput2">Email</label>
	    <input type="text" name="email" value="{{ old('email') }}" class="form-control @if($errors->has('email')) is-invalid @endif" id="formGroupExampleInput2" placeholder="Enter product email">
	    <div class="invalid-feedback"> 
		    @if($errors->has('email')) {{ $errors->first('email') }} @endif
		</div>
	  </div>
	  <div class="form-group">
	    <label for="formGroupExampleInput2">Password</label>
	    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
	  </div>
	  <div class="form-group">
	    <label for="formGroupExampleInput2">Locations</label>
	    <select class="form-control" name="location_id[]">
	    	@foreach($locations as $location)
	    		<option value="{{ $location->id }}">{{ $location->name }}</option>
	    	@endforeach
	    </select>
	  </div>
	   <div class="form-group">
	   		<button type="submit" class="btn btn-primary">Submit</button>
	   </div>
	</form>
  </div>
</div>
</div>

@endsection