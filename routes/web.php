<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
	Route::get('/product/export','ProductController@export');
	Route::get('/product','ProductController@index');
	Route::get('/product/create','ProductController@create');
	Route::post('/product','ProductController@store');
	Route::get('/product/{id}','ProductController@edit');
	Route::patch('/product/{id}','ProductController@update');
	Route::patch('/product/{id}','ProductController@update');
	Route::delete('/product/{id}','ProductController@destroy');

	Route::resource('/category','CategoryController');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
